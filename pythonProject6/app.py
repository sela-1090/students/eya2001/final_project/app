from flask import Flask, render_template, request, redirect, url_for
import psycopg2
app = Flask(__name__)

DB_NAME = 'final_project'
DB_USER = 'EYA_db'
DB_PASSWORD = "a1a2a3"
DB_HOST = "localhost"
DB_PORT = 5432

conn = psycopg2.connect(
    dbname=DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    host=DB_HOST,
    port=DB_PORT
)


ANIMALS = [
    {"name": "Dog", "image_url": "dog.jpeg", "guessed": False},
    {"name": "Cat", "image_url": "cat.jpeg", "guessed": False},
    {"name": "Elephant", "image_url": "elephant.jpg", "guessed": False},
    {"name": "Bird", "image_url": "bird.jpg", "guessed": False}
]

@app.route('/')
def index():
    return render_template('index.html', animals=ANIMALS)

@app.route('/animal/<string:animal_name>', methods=['POST'])
def animal(animal_name):
    guessed_name = request.form['guessed_name']
    for animal in ANIMALS:
        if animal['name'] == animal_name:
            if guessed_name.lower() == animal['name'].lower():
                animal['guessed'] = True
                return redirect(url_for('animal_info', animal_name=animal_name))
            else:
                return redirect(url_for('index'))

@app.route('/animal/<string:animal_name>/info')
def animal_info(animal_name):
    animal = next((a for a in ANIMALS if a['name'] == animal_name), None)
    if animal and animal.get('guessed', False):
        return render_template('info.html', animal=animal)
    else:
        return redirect(url_for('index'))

@app.route('/reset')
def reset_guesses():
    for animal in ANIMALS:
        animal['guessed'] = False
    return redirect(url_for('index'))

if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0', port=5000)
    finally:
        conn.close()
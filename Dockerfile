FROM python:3.9

# Create the app directory
WORKDIR /app

# Copy the requirements.txt file into the Docker build context
COPY requirements.txt /app

# Copy the contents of the pythonProject6 directory to the app directory
COPY pythonProject6 /app/pythonProject6

# Change the working directory to /app/pythonProject6
WORKDIR /app/pythonProject6

# Install dependencies
RUN pip install flask
RUN pip install --no-cache-dir -r /app/requirements.txt
RUN pip install tk
RUN pip install pygame
RUN pip install Pillow
RUN pip install requests
RUN pip install psycopg2

# Set the command to run the app
CMD ["python", "./app.py"]

# Expose the required port
EXPOSE 5000
